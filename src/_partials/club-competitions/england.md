Season: 2022 - 2023

**The FA Player**

The [FA Player](fa_player) streams all matches for the **Women's Super League (WSL)**, all matches for the **Women's Championship (WC)**, some matches for the **FA Cup**, and some matches for the **Continental Tyres League Cup**.

The matches on **[FA Player](fa_player)** will be available to you unless a commercial broadcaster has picked it up in your location. For countries _with_ a commercial broadcaster, matches not picked by the broadcaster will be on the FA Player. For countries _without_ a commercial broadcaster, all matches will be on the FA Player.

**Commercial Broadcasters**

| Watching From | Competition | Broadcasters | Details |
| :------------ | :---------- | :----------- | :------ |
| **UK** | WSL, FA Cup, League Cup | [BBC][bbc] \| [Sky Sports][sky_sports] | selected matches |
| &nbsp; | | | |
| **Australia** | WSL    | [Optus Sport][optus_sport] | all matches |
|               | FA Cup | [Optus Sport][optus_sport] | selected matches |
| &nbsp; | | | |
| **Italy, Japan, Spain** | WSL    | [DAZN][dazn] | 2 matches per round |
|                         | FA Cup | [DAZN][dazn] | selected matches |
| &nbsp; | | | |
| **USA** | WSL & FA Cup | ?? | |

[bbc]: https://www.bbc.co.uk/iplayer
[dazn]: https://www.youtube.com/c/DAZNUEFAWomensChampionsLeague
[fa_player]: https://faplayer.thefa.com/
[optus_sport]: https://sport.optus.com.au/
[sky_sports]: https://www.skysports.com/

Season: 2022 - 2023

| Watching From | Competition | Broadcasters | Details |
| :------------ | :---------- | :----------- | :------ |
| Anywhere | Champions League | [DAZN][dazn] | all matches |

[dazn]: https://www.youtube.com/c/DAZNUEFAWomensChampionsLeague

Season: 2022 - 2023

| Watching From | Competition | Broadcasters | Details |
| :------------ | :---------- | :----------- | :------ |
| Germany | Frauen Bundesliga | [Magenta Sport][magenta_de] | all matches |
| UK & USA | Frauen Bundesliga | [ATA Football][ata_football] | ??? |

[ata_football]: https://atafootball.com/
[magenta_de]: https://www.magentasport.de/

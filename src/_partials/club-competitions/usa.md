Season: 2022

| Watching From | Competition | Broadcasters |
| :------------ | :---------- | :------------------- |
| USA      | NWSL | NWSL Twitch Channels: [1][twitch_nwsl] \| [2][twitch_nwsl2] \| [4][twitch_nwsl4] |
| Anywhere | NWSL | NWSL Twitch Channels: [1][twitch_nwsl] \| [2][twitch_nwsl2] \| [4][twitch_nwsl4] |

[twitch_nwsl]: https://www.twitch.tv/nwslofficial
[twitch_nwsl2]: https://www.twitch.tv/nwslofficial2
[twitch_nwsl4]: https://www.twitch.tv/nwslofficial4

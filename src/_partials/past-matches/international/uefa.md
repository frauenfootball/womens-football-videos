#### European Championship

* 2022 - [Downloads ⬇️](https://drive.google.com/drive/folders/1aPECvQQ5cPvoC3qC-b2bm5Y3yfy-ReJa?usp=sharing) \| [Streams ▶️](https://frauenfootball.tumblr.com/post/689185683141705729/euro-2022)
* 2017 - [Downloads ⬇️](https://drive.google.com/drive/folders/1YxXVLoxWp145aH0NL-0pB2kVQsMlcM5W?usp=sharing)
* 2013 - [Streams ▶️](https://frauenfootball.tumblr.com/post/652408583609253888/euros-2013-matches)

### Nations League

* 2023/2024 - [Downloads ⬇️](https://drive.google.com/drive/folders/12SC_iP5fGncIx6_AzEm5oCRwGLlxZXtx?usp=sharing)
* 2025 - [Downloads ⬇️](https://drive.google.com/drive/folders/1YF6DjgceMUyyQWF7xgvwvcFsH4F-POl2?usp=drive_link)

#### World Cup Qualifiers

* WWC 2023 - [Streams (YouTube Playlist ▶️)](https://www.youtube.com/playlist?list=PL7m1i1vDF1DhhS7B8wJl0N6DzOlPFW4p0)

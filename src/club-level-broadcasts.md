---
layout: layouts/main.njk
page_title: Club-level Broadcasts
content_class: club-level-broadcasts
---

## Club-level League and Tournament Streams/Broadcasts

This serves as a one-page reference of official broadcasters of club-level leagues and tournaments around the world. The streams/broadcasts here are ones verified by the contributor(s) of this website. It is by no means exhaustive. Additionally, only regular streams/broadcasters are included in this list; "regular" meaning that a website/broadcaster shows all or some matches from a league or tournament every week or every round.

For more complete, up-to-date, and round-by-round or per-match lists, please check [Women's Soccer TV](https://www.womenssoccertv.info) or [Live Soccer TV](https://www.livesoccertv.com/).

&nbsp;

Unless otherwise specified, the commercial broadcasters listed below will only air some matches per round of a league/tournament.

&nbsp;

### UEFA

| Watching From | League/Tournament | Broadcasters |
| :------------ | :---------------- | :----------- |
| Anywhere | Champions League | [DAZN](dazn) |

&nbsp;\
&nbsp;

### Australia

Season: 2022 - 2023

&nbsp;

### England

Season: 2022 - 2023

**The FA Player**

The [FA Player](fa_player) streams all matches for the **Women's Super League (WSL)**, all matches for the **Women's Championship (WC)**, some matches for the **FA Cup**, and some matches for the **Continental Tyres League Cup**.

The matches on **[FA Player](fa_player)** will be available to you unless a commercial broadcaster has picked it up in your location. For countries _with_ a commercial broadcaster, matches not picked by the broadcaster will be on the FA Player. For countries _without_ a commercial broadcaster, all matches will be on the FA Player.

**Commercial Broadcasters**

| Watching From | League/Tournament | Broadcasters |
| :------------ | :---------------- | :----------- |
| **UK** | WSL, FA Cup, League Cup | [BBC](bbc) \| [Sky Sports](sky_sports) |
| &nbsp; | &nbsp; | &nbsp; |
| **Australia** | WSL    | [Optus Sport](optus_sport), all matches  |
|               | FA Cup | [Optus Sport](optus_sport)               |
| &nbsp; | &nbsp; | &nbsp; |
| **Italy, Japan, Spain** | WSL & FA Cup | [DAZN](dazn) |
| &nbsp; | &nbsp; | &nbsp; |
| **USA** | WSL & FA Cup | ?? |

&nbsp;

### France

Season: 2022 - 2023

&nbsp;

### Germany

Season: 2022 - 2023

| Watching From | League/Tournament | Broadcasters |
| :------------ | :---------------- | :----------- |
| Germany | Frauen Bundesliga | [Magenta Sport](magenta_de), all matches |
| UK & USA | Frauen Bundesliga | [ATA Football](ata_football) |

&nbsp;

### Italy

Season: 2022 - 2023

&nbsp;

### Spain

Season: 2022 - 2023

| Watching From | League/Tournament | Broadcasters |
| :------------ | :---------------- | :------------ |
| Anywhere | Liga F | [DAZN](dazn) |

&nbsp;

### Sweden

Season: 2023

&nbsp;

### USA

Season: 2023

| Watching From | League/Tournament | Streams/Broadcasts |
| :------------ | :------------------ | :------------------- |
| USA           | NWSL              | NWSL Twitch Channels: [1](twitch_nwsl) \| [2](twitch_nwsl2) \| [4](twitch_nwsl4) |
| Anywhere      | NWSL              | NWSL Twitch Channels: [1](twitch_nwsl) \| [2](twitch_nwsl2) \| [4](twitch_nwsl4) |

&nbsp;

----

[Back to Top](#)

[ata_football]: https://atafootball.com/
[bbc]: https://www.bbc.co.uk/iplayer
[dazn]: https://www.youtube.com/c/DAZNUEFAWomensChampionsLeague
[fa_player]: https://faplayer.thefa.com/
[magenta_de]: https://www.magentasport.de/
[optus_sport]: https://sport.optus.com.au/
[sky_sports]: https://www.skysports.com/
[twitch_nwsl]: https://www.twitch.tv/nwslofficial
[twitch_nwsl2]: https://www.twitch.tv/nwslofficial2
[twitch_nwsl4]: https://www.twitch.tv/nwslofficial4

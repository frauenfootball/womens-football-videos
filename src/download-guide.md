---
layout: layouts/main.njk
page_title: Google Drive Download Guide
---

## Google Drive Download Guide

### Important: Please _DOWNLOAD_ the videos to your computer

DO NOT STREAM/PLAY THE VIDEO FROM THE GOOGLE DRIVE FOLDER.

While you maybe be able stream/play the video from Google Drive, I strongly urge you to _download_ them. Streams/playbacks contribute to reaching the limits much faster than downloads. You can also work around the download limits.

### Log in to Google to Download the Videos

Some of Google's limits are lifted if you are logged in.

If you still run into those issues while logged in, please check the workarounds below. (Make sure that you are logged in!)

### "File has exceeded daily limit" error

1. Right-click and choose “Make a Copy”.
2. Wait until you see the notification on the bottom left of the page that a copy has been created.
3. Click “Show File Location” OR go to your own Google Drive.
4. Find the file you copied and download it. The file will be named “Copy of [the original file name]”

### “Error creating copy” error

If you tried to copy the file and ran into the _“Error creating copy”_ error:

1. Right click on the file and click “Add Shortcut to Drive”
2. Save the shortcut under a folder in your own Google Drive.
3. Download the whole folder where you saved the shortcut in. This will zip the folder, so it will take some time to prepare the download.

---
layout: layouts/main.njk
page_title: Past Matches - Club Competitions
page_css: "/assets/css/past-matches.css"
content_class: past-matches
---

## Past Matches - Club Competitions

This page contains download links or replay streams to past club-level matches.

Where possible, please check out the streams from official sources first so they know there is interest!

**UEFA Champions League** - [Downloads ⬇️](https://drive.google.com/drive/folders/1nJtRgG82hubLd7Xjw9KjP2ZHBNHe24Ne?usp=sharing)

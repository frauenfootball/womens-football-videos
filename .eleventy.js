const markdownIt = require('markdown-it')
const markdownItAnchor = require('markdown-it-anchor')
const markdownItAttrs = require('markdown-it-attrs')

module.exports = function(eleventyConfig) {
  // Copy the `assets/` directory
  eleventyConfig.addPassthroughCopy("assets")

  eleventyConfig.setLibrary("md", markdownIt().use(markdownItAnchor).use(markdownItAttrs))

  eleventyConfig.addNunjucksShortcode(
    "markdown",
    content => `<div class="md-block">${markdownIt({ html: true }).render(content)}</div>`
  );

  return {
    dir: {
      input: 'src',
      output: 'public',
    }
  }
}
